<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">    
    <meta name="description" content="">
    <meta name="author" content="">
    <title>CineSite Administracion | Bienvenido</title>    

	<spring:url value="/resources" var="urlPublic"></spring:url>
	<spring:url value="/" var="urlRoot" />
    
    <link href="${urlPublic}/bootstrap/css/bootstrap.min.css" rel="stylesheet">    
	<link href="${urlPublic}/bootstrap/css/theme.css" rel="stylesheet">

  </head>

  <body>

    <jsp:include page="includes/menu.jsp"></jsp:include>

    <div class="container theme-showcase" role="main">

      <div class="jumbotron">         
        <h3>Administración del Sistema</h3>
        <!-- Uso de tag sec:authentication y property principal.username para obtener usuario logueado 
        	Esta es la manera de recuperar el usuario en una vista. -->
        <p>Bienvenido(a) <sec:authentication property="principal.username"/> </p>
      </div>

      <jsp:include page="includes/footer.jsp"></jsp:include>	

    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
    <script src="${urlPublic}/bootstrap/js/bootstrap.min.js"></script> 
    <script>
      function goBack() {
         window.history.back();
      }
    </script>
  </body>
</html>
