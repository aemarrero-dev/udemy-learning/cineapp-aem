
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!-- Spring Security Tags -->
<spring:url value="/" var="urlRoot" />

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">

		<!-- Menu de opciones para usuarios no registrados -->
		<sec:authorize access="isAnonymous()">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="${urlRoot}">My CineSite</a>
			</div>

			<!-- Opciones de menu -->
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="${urlRoot}about">Acerca</a></li>
					<li><a href="${urlRoot}formLogin">Login</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</sec:authorize>

		<!-- Menu de opciones para usuarios con rol EDITOR -->
		<sec:authorize access="hasAnyAuthority('EDITOR')">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span>
			</button>
			<!-- admin/index lo incluimos aqui para que al hacer clic en My CineSIte me redireccione a la bienvenida -->
			<a class="navbar-brand" href="${urlRoot}admin/index">My CineSite | Administracion</a>
		</div>

		<!-- Opciones de menu -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				
				<!-- Uso de paginacion -->
				<li><a href="${urlRoot}peliculas/indexPaginate?page=0">Peliculas</a></li>
				<li><a href="${urlRoot}horarios/indexPaginate?page=0">Horarios</a></li>
				<li><a href="${urlRoot}noticias/index">Noticias</a></li>
				<li><a href="${urlRoot}admin/logout">Salir</a></li>
			</ul>
		</div>
		<!--/.nav-collapse -->
		</sec:authorize>
		
		<!-- Menu de opciones para usuarios con rol GERENTE -->
		<sec:authorize access="hasAnyAuthority('GERENTE')">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<!-- admin/index lo incluimos aqui para que al hacer clic en My CineSIte me redireccione a la bienvenida -->
			<a class="navbar-brand" href="${urlRoot}admin/index">My CineSite |Administracion</a>
		</div>

		<!-- Opciones de menu -->
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<!-- <li><a href="${urlRoot}peliculas/index">Peliculas</a></li> -->
				<!-- Uso de paginacion -->
				<li><a href="${urlRoot}peliculas/indexPaginate?page=0">Peliculas</a></li>
				<li><a href="${urlRoot}horarios/indexPaginate?page=0">Horarios</a></li>
				<li><a href="${urlRoot}noticias/index">Noticias</a></li>
				<li><a href="${urlRoot}banners/index">Banners</a></li>
				<li><a href="${urlRoot}usuarios/create">Crear Usuario</a></li>
				<li><a href="${urlRoot}admin/logout">Salir</a></li>
			</ul>
		</div>
		<!--/.nav-collapse -->
		</sec:authorize>

	</div>
</nav>