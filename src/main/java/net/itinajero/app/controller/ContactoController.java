package net.itinajero.app.controller;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import net.itinajero.app.model.Contacto;
import net.itinajero.app.service.IPeliculasService;

@Controller
public class ContactoController {

	@Autowired
	private IPeliculasService servicePeliculas;
	/**
	 * Al crearse una peticion a contacto, Spring automaticamente crear� un objeto Contacto y lo incluye en el modelo.
	 * Si deseamos que el nombre del objeto sea distinto a contacto, se especifica en el ModelAttribute y debe coincidir con el path de la vista.
	 * */
	@GetMapping("/contacto")
	public String mostarFormulario(@ModelAttribute("instanciaContacto") Contacto contacto, Model model) {
		model.addAttribute("generos", servicePeliculas.buscarGeneros());
		model.addAttribute("tipos", tipoNotificaciones());
		return "formContacto";
	}
	
	@PostMapping("/contacto")
	public String guardar(@ModelAttribute("instanciaContacto") Contacto contacto) {
		System.out.println("contacto: " + contacto);
		/*model.addAttribute("generos", servicePeliculas.buscarGeneros());
		model.addAttribute("tipos", tipoNotificaciones());
		return "formContacto";*/
		return "redirect:/contacto";
	}
	
	private List<String> tipoNotificaciones(){
		List<String> notificaciones = new LinkedList<String>();
		notificaciones.add("Estrenos");
		notificaciones.add("Promociones");
		notificaciones.add("Noticias");
		notificaciones.add("Preventas");
		return notificaciones;
	}
	
}
