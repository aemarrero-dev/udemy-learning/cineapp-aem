package net.itinajero.app.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.itinajero.app.model.Banner;
import net.itinajero.app.model.Horario;
import net.itinajero.app.model.Noticia;
import net.itinajero.app.model.Pelicula;
import net.itinajero.app.service.IBannersService;
import net.itinajero.app.service.IHorariosService;
import net.itinajero.app.service.INoticiasService;
import net.itinajero.app.service.IPeliculasService;
import net.itinajero.app.util.Utileria;

@Controller
public class HomeController {
	
	@Autowired
	private IPeliculasService servicePelicula;
	
	@Autowired
	private IBannersService serviceBanner;
	
	@Autowired
	private IHorariosService serviceHorarios;
	
	// Inyectamos una instancia desde nuestro Root ApplicationContext
	@Autowired
	private INoticiasService serviceNoticias;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

	@RequestMapping(value="/search", method=RequestMethod.POST)
	public ModelAndView buscar(@RequestParam String fecha) { //, Model model){ 
		System.out.println("Buscando las peliculas para la fecha: " + fecha);
		
		//PELICULAS
		List<Pelicula> peliculas = servicePelicula.buscarTodas();
		List<String> listaFechas = Utileria.getNextDays(4);
		
		//BANNERS
		List<Banner> banners = serviceBanner.buscarTodos();
		/*model.addAttribute("fechas", listaFechas);
		model.addAttribute("fechaBusqueda", fecha);
		model.addAttribute("peliculas", peliculas);*/
		
		 ModelAndView model = new ModelAndView();
		 model.addObject("fechas", listaFechas);
		 model.addObject("fechaBusqueda", fecha);
		 model.addObject("peliculas", peliculas);
		 model.addObject("banners", banners);
		 model.setViewName("home");
		
		 return model;
		
		//return "home";
	}
	
	@RequestMapping(value="/", method=RequestMethod.GET)
	public String mostrarPrincipal(Model model){
		//List<Pelicula> peliculas = getLista();
		
		try {
			Date fechaSinHora = sdf.parse(sdf.format(new Date()));
			System.out.println(fechaSinHora);
			List<String> listaFechas = Utileria.getNextDays(4);
			
			//PELICULAS
			List<Pelicula> peliculas = servicePelicula.buscarTodas();
			//List<Pelicula> peliculas = servicePelicula.buscarPorFecha(fechaSinHora); //Filtrando por fecha actual
			
			model.addAttribute("fechas", listaFechas);
			model.addAttribute("fechaBusqueda", sdf.format(new Date()));
			model.addAttribute("peliculas", peliculas);
			//BANNERS - Reemplazado mas abajo con @ModelAttribute
			//List<Banner> banners = serviceBanner.buscarTodos();
			//model.addAttribute("banners", banners);
			
		} catch (ParseException e) {
			System.out.println("Error: HomeController.mostrarPrincipal" + e.getMessage());
		}

		return "home";
	}
	
	//@RequestMapping(value="/detail/{id}/{fecha}", method=RequestMethod.GET)
	//
	//public String mostrarDetalle(Model model, @RequestParam("idMovie") int idPelicula, @RequestParam("fecha") Date fecha){
	@RequestMapping(value="/detail/{id}/{fecha}", method=RequestMethod.GET)
	public String mostrarDetalle(Model model, @PathVariable("id") int idPelicula, @PathVariable("fecha") Date fecha){
		
		//IMPORTANTE: cambiamos el parametro fecha a Date y debemos configurar el databinding para los tipo Date. InitBinder
		List<Horario> horarios = serviceHorarios.buscarPorIdPelicula(idPelicula, fecha);
		model.addAttribute("pelicula", servicePelicula.buscarPorId(idPelicula));
		model.addAttribute("horarios", horarios);
		model.addAttribute("fechaBusqueda", sdf.format(fecha));

		return "detalle";
	}
	
	//Para mostrar nuestra propia vista de Login y no la de Spring automatica.
	@RequestMapping(value = "/formLogin")
	public String mostrarLogin(){
		return "formLogin";
	}
	
	@ModelAttribute("noticias")
	public List<Noticia> getNoticias(){
		return serviceNoticias.buscarUltimas();
	}
	
	
	@ModelAttribute("banners")
	public List<Banner> getBanners(){
		return serviceBanner.buscarActivos();
	}
	
	@RequestMapping(value = "/about")
	public String mostrarAcerca() {			
		return "acerca";
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		
		binder.registerCustomEditor(Date.class, new CustomDateEditor(sdf, false));
	}
}
