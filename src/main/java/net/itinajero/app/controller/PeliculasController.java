package net.itinajero.app.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.itinajero.app.model.Pelicula;
import net.itinajero.app.service.IDetallesService;
import net.itinajero.app.service.IPeliculasService;
import net.itinajero.app.util.Utileria;

@Controller
@RequestMapping("/peliculas")
public class PeliculasController {

	@Autowired
	private IDetallesService detallePeliculas;
	
	@Autowired
	private IPeliculasService servicePeliculas;
	
	@GetMapping("/index")
	public ModelAndView mostrarIndex() {
		List<Pelicula> lista = servicePeliculas.buscarTodas();
		ModelAndView model = new ModelAndView();
		model.addObject("peliculas", lista);
		model.setViewName("peliculas/listPeliculas");
		return model;
	}
	
	//Uso de paginacion
	@GetMapping("/indexPaginate")
	public ModelAndView mostrarIndexPaginado(Pageable page) {
		Page<Pelicula> lista = servicePeliculas.buscarTodas(page);
		ModelAndView model = new ModelAndView();
		model.addObject("peliculas", lista);
		model.setViewName("peliculas/listPeliculas");
		return model;
	}
	
	//ESTE METODO ANTES SOLO RETORNABA LA VISTA FORMPELICULA y sin parametros. AHORA RECIBE ModelAttribe Pelicula
	@GetMapping("/create")
	public ModelAndView crear(@ModelAttribute Pelicula pelicula) {
		ModelAndView model = new ModelAndView();
		//model.addObject("generos", servicePeliculas.buscarGeneros()); //Reemplazado por getGeneros
		model.setViewName("/peliculas/formPelicula");
		return model;
	}
	
	//Se incluy� @ModelAttribute a Pelicula
	// Cuando se ingresa un valor erroneo, los valores ingresados se mantienen y no se borran como hacia antes.
	@PostMapping("/save")
	public String guardar(@ModelAttribute Pelicula pelicula, BindingResult result, RedirectAttributes attributes,
			@RequestParam("archivoImagen") MultipartFile multipart, HttpServletRequest request, Model model) {
	
		if(result.hasErrors()) {
			System.out.println("surgieron ERRORES.");
			//Cuando ocurre se perdia los valores del genero, por eso se carga al model aqui.
			//model.addAttribute("generos", servicePeliculas.buscarGeneros());  //Reemplazado por getGeneros
			return "peliculas/formPelicula";
		}
		
		if(!multipart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multipart, request);
			System.out.println("guardarImagen: " + nombreImagen);
			pelicula.setImagen(nombreImagen);
		}
		/*
		for(ObjectError error : result.getAllErrors()) {
			System.out.println(error.getDefaultMessage());
		}
		*/
		
		//con Model, el mensaje no se visualiza en la vista ya que se hizo un redirect. Se usa redirectAttributes
		//model.addAttribute("mensaje", "Registro guardado.");
		
		attributes.addFlashAttribute("mensaje", "Registro Guardado");
		
		//System.out.println("Nro anterior de peliculas: " + servicePeliculas.buscarTodas().size());
		
		//Si id de pelicula o detalle es nuevo, se ejecuta un insert, sino update.
		detallePeliculas.insertar(pelicula.getDetalle());
		servicePeliculas.insertar(pelicula);
		
		//System.out.println("Nro actual de peliculas: " + servicePeliculas.buscarTodas().size());
		//return "peliculas/formPelicula";
		return "redirect:/peliculas/index";
	}

	@GetMapping(value="/edit/{id}")
	public ModelAndView mostrarDetalle(@PathVariable("id") int idPelicula){
		Pelicula p = servicePeliculas.buscarPorId(idPelicula);
		ModelAndView model = new ModelAndView();
		model.addObject("pelicula", p);
		//model.addObject("generos", servicePeliculas.buscarGeneros());  //Reemplazado por getGeneros
		model.setViewName("/peliculas/formPelicula");
		return model;
	}
	
	@GetMapping(value="/delete/{id}")
	public String eliminar(@PathVariable("id") int idPelicula, RedirectAttributes attributes) {
		
		
		//Buscamos la pelicula para obtener su detalle y luego la eliminamos.
		Pelicula p = servicePeliculas.buscarPorId(idPelicula);
		servicePeliculas.eliminar(idPelicula);
		detallePeliculas.eliminar(p.getDetalle().getId());
		
		attributes.addFlashAttribute("mensaje", "Pelicula Eliminada");
		return "redirect:/peliculas/index"; //Redireccionamos a listPeliculas
	}
	
	/*
	 * @ModelAttribute hace disponible el atributo generos en todos los metodos de este controlador.
	 * Ya no se usaria en los metodos mostrarDetalle, crear, guardar.
	 * */
	@ModelAttribute("generos")
	public List<String> getGeneros(){
		return servicePeliculas.buscarGeneros();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

}
