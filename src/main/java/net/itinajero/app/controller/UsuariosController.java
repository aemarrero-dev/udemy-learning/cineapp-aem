package net.itinajero.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.itinajero.app.model.Perfil;
import net.itinajero.app.model.Usuario;
import net.itinajero.app.service.IPerfilesService;
import net.itinajero.app.service.IUsuariosService;

/* ESTA CLASE ES SOLO PARA ENCRIPTAR LAS CLAVES DE LOS USUARIOS Y PODER VALIDAR EL FUNCIONAMIENTO
 * DEL ALGORITMO BCRYPT IMPLEMENTADO EN SECURITY.XML (SPRING SECURITY).
 * ENCRIPTAMOS TODAS LAS CLAVES DE LOS USUARIOS Y LAS GUARDAMOS ASI EN LA BD.
 * 
 * */
@Controller
@RequestMapping("/usuarios")
public class UsuariosController {
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private IUsuariosService serviceUsuarios;
	
	@Autowired
	private IPerfilesService servicePerfiles;
	
	@GetMapping("/create")
	public String crear(@ModelAttribute Usuario usuario) { //@ModelAttribute enlaza Usuario con formulario
		return "usuarios/formUsuario";
	}
	
	@GetMapping("/index")
	public String index() {
		return "usuarios/listUsuarios";
	}
	
	//campo perfil no es de Usuario, por eso lo obtenemos con RequestParam.
	@PostMapping("/save")
	public String guardar(@ModelAttribute Usuario usuario, @RequestParam("perfil") String perfil) {
		System.out.println(usuario);
		System.out.println("perfil: " + perfil);
		
		String passwordUsuario = usuario.getPwd();
		String passwordEncriptada = encoder.encode(passwordUsuario);
		usuario.setPwd(passwordEncriptada);
		usuario.setActivo(1);
		serviceUsuarios.guardar(usuario);
		
		Perfil perfilObj = new Perfil();
		perfilObj.setCuenta(usuario.getCuenta());
		perfilObj.setPerfil(perfil);
		
		servicePerfiles.guardar(perfilObj);
		
		return "redirect:/usuarios/index";	
	}
	
	@GetMapping("/demo-bcrypt")
	public String pruebaBcrypt() {
		String password = "luis123";
		String encriptado = encoder.encode(password);
		System.out.println("Luis - Password encriptado: " + encriptado);
		
		password = "andres123";
		encriptado = encoder.encode(password);
		System.out.println("Andres - Password encriptado: " + encriptado);
		
		password = "mari123";
		encriptado = encoder.encode(password);
		System.out.println("Marisol - Password encriptado: " + encriptado);
		return "usuarios/demo";
	}
	
}
