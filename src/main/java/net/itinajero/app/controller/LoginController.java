package net.itinajero.app.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class LoginController {
	
	@GetMapping(value = "/logout")
	public String logout(HttpServletRequest request) {
		SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
		logoutHandler.logout(request, null, null);
		
		return "redirect:/formLogin"; // ventana personalizada de login
		
		//return "redirect:/login"; //ventana login por default de spring
	}
	
	@GetMapping(value="/index")
	public String mostrarPrincipalAdmin(Authentication auth) {
		//Para obtener el usuario desde el controlador usamos el objeto Authentication. Util para logs o monitoreo.
		System.out.println("Username: " + auth.getName());
		
		//Recuperar los roles del usuario
		for (GrantedAuthority rol : auth.getAuthorities()) {
			System.out.println("Rol: " + rol);
		}
		return "admin";
	}
}
