package net.itinajero.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="noticias")
public class Noticia {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	/**
	 Si el nombre de nuestro atributo en nuestra entidad mapea con un campo de una tabla y tienen el mismo nombre, 
	 en este caso se puede omitir la anotaci�n @Column. Esa es la raz�n por la cual en nuestra clase de modelo Noticia 
	 no hemos utilizado la anotaci�n @Column en los atributos de la clase.
	 * */
	//@Column(name="titulo",length=250,nullable=false)
	private String titulo;
	
	//@Column(name="fechaRegistro")
	private Date fecha;
	private String detalle;
	private String estatus;
	
	public Noticia() {
		super();
		this.fecha = new Date();
		this.estatus = "Activa";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@Override
	public String toString() {
		return "Noticia [id=" + id + ", titulo=" + titulo + ", fecha=" + fecha + ", detalle=" + detalle + ", estatus="
				+ estatus + "]";
	}
}
