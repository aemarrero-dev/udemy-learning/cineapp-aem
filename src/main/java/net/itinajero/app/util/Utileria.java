package net.itinajero.app.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.multipart.MultipartFile;

public class Utileria {
	
	//DEVUELVE LISTADO CON LOS CINCO DIAS SIGUIENTE A LA FECHA ACTUAL. 
	public static List<String> getNextDays(int count){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		//Today
		Date start = new Date();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, count); //next N days from now
		Date endDate = cal.getTime();
		
		GregorianCalendar gcal = new GregorianCalendar();
		gcal.setTime(start);
		List<String> nextDays = new ArrayList<String>();
		while(!gcal.getTime().after(endDate)) {
			Date d = gcal.getTime();
			gcal.add(Calendar.DATE, 1);
			nextDays.add(sdf.format(d));
		}
		return nextDays;
	}
	
	public static String guardarImagen(MultipartFile multiPart, HttpServletRequest request) {
		//Obtenemos el nombre original del archivo
		String nombreOriginal = multiPart.getOriginalFilename();
		//Reemplazamos en el nombre de archivo los espacios por guiones
		String nombreFinal = nombreOriginal.replace(" ", "-");
		//Obtenemos ruta absoluta de directorio images
		//apache-tomcat/webapps/cineapp/resources/images
		String rutaFinal = request.getServletContext().getRealPath("/resources/images/");
		System.out.println("rutaFinal: " + rutaFinal);
		
		System.out.println(multiPart.getContentType());
		if("image/png".equalsIgnoreCase(multiPart.getContentType()) || 
		   "image/jpeg".equalsIgnoreCase(multiPart.getContentType())) {
			// Agregamos al nombre del archivo 8 caracteres aleatorios para evitar duplicados.
			nombreFinal = randomAlphaNumeric(8)+nombreFinal;
			try {
				// Formamos el nombre del archivo para guardarlo en el disco duro
				File imageFile = new File(rutaFinal + nombreFinal);
				// Aqui se guarda fisicamente el archivo en el disco duro
				System.out.println("file: " + imageFile.getAbsolutePath());
				multiPart.transferTo(imageFile);
			} catch (IOException e) {
				System.out.println("Error " + e.getMessage());
				nombreFinal = null;
			}
		} else {
			nombreFinal = "cinema.png";
		}
		return nombreFinal;
	}
	
	//Metodo para generar una cadena de longitud N de caracteres aleatorios.
	public static String randomAlphaNumeric(int count) {
		String CARACTERES = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder builder = new StringBuilder();
		while (count-- != 0) {
			int character = (int) (Math.random() * CARACTERES.length());
			builder.append(CARACTERES.charAt(character));
		}
		return builder.toString();
	}
	/*
	public String guardarImagen(MultipartFile multiPart, HttpServletRequest request) {
		// Obtenemos el nombre original del archivo
		String nombreOriginal = multiPart.getOriginalFilename();
		System.out.println("nombreOriginal: " + nombreOriginal);
		int lastIndex = nombreOriginal.lastIndexOf("\\");
		
		System.out.println("content-type: " + multiPart.getContentType());
		boolean esImagen=true;
		String nombreImagen="", nombreFinal="";
		if(!"image/png".equalsIgnoreCase(multiPart.getContentType())) {
			nombreFinal = "cinema.png";
			esImagen = false;
			//nombreFinal = randomAlphaNumeric(8)+ "cinema.png";
		} else {
			//nombreImagen = nombreOriginal.substring(lastIndex+1, nombreOriginal.length()); 
			nombreImagen = nombreOriginal.substring(lastIndex+1, nombreOriginal.length()); 
			System.out.println("nombreOriginal 2: " + nombreImagen);
			// Reemplazamos en el nombre de archivo los espacios por guiones
			nombreImagen = nombreImagen.replace(" ", "-");
			// Agregamos al nombre del archivo 8 caracteres aleatorios para evitar duplicados.
			nombreFinal = randomAlphaNumeric(8)+nombreImagen;
			// Obtenemos la ruta ABSOLUTA del directorio images
			// apache-tomcat/webapps/cineapp/resources/images/
		}
		
		if(esImagen) {
			String rutaFinal = request.getServletContext().getRealPath("/resources/images/");
			try {
				// Formamos el nombre del archivo para guardarlo en el disco duro
				File imageFile = new File(rutaFinal + nombreFinal);
				System.out.println("ImageFile: " + imageFile);
				// Aqui se guarda fisicamente el archivo en el disco duro
				multiPart.transferTo(imageFile);
				return nombreFinal;
			} catch (IOException e) {
				System.out.println("Error " + e.getMessage());
				return null;
			}
		}
		return nombreFinal;
	}*/
	
	
}
