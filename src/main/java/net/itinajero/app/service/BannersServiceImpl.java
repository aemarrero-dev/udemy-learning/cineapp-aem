package net.itinajero.app.service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

//import org.springframework.stereotype.Service;

import net.itinajero.app.model.Banner;

//@Service
public class BannersServiceImpl implements IBannersService {

	private List<Banner> lista = null;
	
	
	public BannersServiceImpl() {

		lista = new LinkedList<>();
		
		Banner b1 = new Banner();
		b1.setId(1);
		b1.setTitulo("slide1");
		b1.setFecha(new Date());
		b1.setArchivo("slide1.jpg");
		
		Banner b2 = new Banner();
		b2.setId(2);
		b2.setTitulo("slide2");
		b2.setFecha(new Date());
		b2.setArchivo("slide2.jpg");
		
		Banner b3 = new Banner();
		b3.setId(3);
		b3.setTitulo("slide3");
		b3.setFecha(new Date());
		b3.setArchivo("slide3.jpg");
		
		Banner b4 = new Banner();
		b4.setId(4);
		b4.setTitulo("slide4");
		b4.setFecha(new Date());
		b4.setArchivo("slide4.jpg");
		
		Banner b5 = new Banner();
		b5.setId(5);
		b5.setTitulo("slide5");
		b5.setFecha(new Date());
		b5.setArchivo("slide5.jpg");
		
		Banner b6 = new Banner();
		b6.setId(6);
		b6.setTitulo("slide6");
		b6.setFecha(new Date());
		b6.setArchivo("slide6.jpg");
		
		Banner b7 = new Banner();
		b7.setId(7);
		b7.setTitulo("slide7");
		b7.setFecha(new Date());
		b7.setArchivo("slide7.jpg");
		
		lista.add(b1);
		lista.add(b2);
		lista.add(b3);
		lista.add(b4);
	}

	@Override
	public void insertar(Banner banner) {
		lista.add(banner);
		
	}

	@Override
	public List<Banner> buscarTodos() {
		return lista;
	}

	@Override
	public List<Banner> buscarActivos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminar(int idBanner) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Banner buscarPorId(int idBanner) {
		// TODO Auto-generated method stub
		return null;
	}

}
