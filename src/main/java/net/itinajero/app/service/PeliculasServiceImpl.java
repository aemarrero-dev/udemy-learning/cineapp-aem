package net.itinajero.app.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

//import org.springframework.stereotype.Service;

import net.itinajero.app.model.Detalle;
import net.itinajero.app.model.Pelicula;

//@Service
//Comentamos Service para que no haya conflicto con PeliculasServiceJPA
public class PeliculasServiceImpl implements IPeliculasService {

	private List<Pelicula> lista = null;
	public PeliculasServiceImpl() {
		System.out.println("Peliculas from serviceImpl");
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		try {
			lista = new LinkedList<>();
			
			Pelicula p1 = new Pelicula();
			p1.setId(1);
			p1.setTitulo("Power Rangers");
			p1.setDuracion(120);
			p1.setClasificacion("B");
			p1.setGenero("Aventura");
			p1.setFechaEstreno(formatter.parse("02-05-2017"));
			
			Detalle d1 = new Detalle();
			d1.setId(1);
			d1.setActores("Chang Liu, Chris Evans, Walter Bentley");
			d1.setDirector("Akiro Okinawa");
			d1.setSinopsis("Jovenes obtienen superpoderes y protegen al mundo de peligrosos alienigenas");
			d1.setTrailer("https://www.youtube.com/embed/yjpmiqh4wg0");
			
			p1.setDetalle(d1);
			
			Pelicula p2 = new Pelicula();
			p2.setId(2);
			p2.setTitulo("La bella y la bestia");
			p2.setDuracion(132);
			p2.setClasificacion("A");
			p2.setGenero("Infantil");
			p2.setFechaEstreno(formatter.parse("20-05-2017"));
			p2.setImagen("bella.png");
			
			
			Detalle d2 = new Detalle();
			d2.setId(2);
			d2.setActores("Mel Gibson, Meryl Streep, Susan Sarandon");
			d2.setDirector("Quentin Tarantino");
			d2.setSinopsis("Un amor nace entre una bestia y una doncella, pero ella no sabe que es un principe.");
			d2.setTrailer("https://www.youtube.com/embed/XpMjfUJ1lUc");
			
			p2.setDetalle(d2);
			
			Pelicula p3 = new Pelicula();
			p3.setId(3);
			p3.setTitulo("Contratiempo");
			p3.setDuracion(106);
			p3.setClasificacion("B");
			p3.setGenero("Thriller");
			p3.setFechaEstreno(formatter.parse("28-05-2017"));
			p3.setImagen("contratiempo.png");
			
			Detalle d3 = new Detalle();
			d3.setId(3);
			d3.setActores("Will Smith, Danny Glover, Josh Brolin");
			d3.setDirector("Riddley Scott");
			d3.setSinopsis("Accion policial, mujeres y corrupcion contra el tiempo ante un suceso mayor.");
			d3.setTrailer("https://www.youtube.com/embed/Fo6-sfYjn1s");
			
			p3.setDetalle(d3);
			
			Pelicula p4 = new Pelicula();
			p4.setId(4);
			p4.setTitulo("Kong La Isla Calavera");
			p4.setDuracion(118);
			p4.setClasificacion("B");
			p4.setGenero("Accion y Aventura");
			p4.setFechaEstreno(formatter.parse("28-05-2017"));
			p4.setImagen("kong.png");
			p4.setEstatus("Inactiva");
			
			Detalle d4 = new Detalle();
			d4.setId(4);
			d4.setActores("Al Pacino, Robert De Niro, Justin Timberlake");
			d4.setDirector("Peter Jackson");
			d4.setSinopsis("Una expedicion a una isla remota donde habita un animal de grandes magnitudes.");
			d4.setTrailer("https://www.youtube.com/embed/8wP_3OO3Res");
			
			p4.setDetalle(d4);
			
			Pelicula p5 = new Pelicula();
			p5.setId(5);
			p5.setTitulo("Life: Vida Inteligente");
			p5.setDuracion(104);
			p5.setClasificacion("B");
			p5.setGenero("Drama");
			p5.setFechaEstreno(formatter.parse("10-06-2017"));
			p5.setImagen("estreno5.png");
			p5.setEstatus("Activa");
			
			Detalle d5 = new Detalle();
			d5.setId(5);
			d5.setActores("Eddie Murphy, Tom Hanks, Tyrese Gibson");
			d5.setDirector("Steven Spielberg");
			d5.setSinopsis("Viaje al espacio sin imaginar lo que les espera.");
			d5.setTrailer("https://www.youtube.com/embed/vSulYekdrdE");
			
			//Agregamos pelicula a la lista;
			lista.add(p1);
			lista.add(p2);
			lista.add(p3);
			lista.add(p4);
			lista.add(p5);
			
		}catch(ParseException pe) {
			System.out.println("Error: " + pe.getMessage());
		}
	}

	@Override
	public List<Pelicula> buscarTodas() {
		return lista;
	}

	@Override
	public Pelicula buscarPorId(int idPelicula) {
		for (Pelicula p : lista) {
			if(p.getId() == idPelicula) {
				return p;
			}
		}
		return null;
	}

	@Override
	public void insertar(Pelicula pelicula) {
		lista.add(pelicula);
		
	}

	@Override
	public List<String> buscarGeneros() {
		List<String> generos = new LinkedList<>();
		
		generos.add("Accion");
		generos.add("Aventura");
		generos.add("Clasicas");
		generos.add("Comedia Romantica");
		generos.add("Drama");
		generos.add("Terror");
		generos.add("Infantil");
		generos.add("Accion y Aventura");
		generos.add("Romantica");
		generos.add("Ciencia Ficcion");
		return generos;
	}

	@Override
	public void eliminar(int pelicula) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Page<Pelicula> buscarTodas(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pelicula> buscarPorFecha(Date fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Pelicula> buscarActivas() {
		// TODO Auto-generated method stub
		return null;
	}
}
