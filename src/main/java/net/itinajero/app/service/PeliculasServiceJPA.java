package net.itinajero.app.service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.itinajero.app.model.Horario;
import net.itinajero.app.model.Pelicula;
import net.itinajero.app.repository.HorariosRepository;
import net.itinajero.app.repository.PeliculasRepository;

//Implementacion de IPeliculasService pero con JPA
@Service
public class PeliculasServiceJPA implements IPeliculasService {
	
	@Autowired
	private PeliculasRepository peliculaRepo;
	
	@Autowired	
	private HorariosRepository horariosRepo;
	
	@Override
	public void insertar(Pelicula pelicula) {
		peliculaRepo.save(pelicula);
	}

	@Override
	public List<Pelicula> buscarTodas() {
		return peliculaRepo.findAll();
	}

	@Override
	public Pelicula buscarPorId(int idPelicula) {
		Optional<Pelicula> p = peliculaRepo.findById(idPelicula);
		if(p.isPresent()) {
			return p.get();
		}
		return null;
	}
	
	@Override
	public void eliminar(int idPelicula) {
		peliculaRepo.deleteById(idPelicula);
	}
	
	@Override
	public List<String> buscarGeneros() {
		List<String> generos = new LinkedList<>();
		generos.add("Accion");
		generos.add("Aventura");
		generos.add("Clasicas");
		generos.add("Comedia Romantica");
		generos.add("Drama");
		generos.add("Terror");
		generos.add("Infantil");
		generos.add("Accion y Aventura");
		generos.add("Romantica");
		generos.add("Ciencia Ficcion");
		return generos;
	}

	//Uso de paginacion
	@Override
	public Page<Pelicula> buscarTodas(Pageable page) {
		return peliculaRepo.findAll(page);
	}

	@Override
	public List<Pelicula> buscarPorFecha(Date fecha) {
		List<Pelicula> peliculas = null;
		// Buscamos en la tabla de horarios, [agrupando por idPelicula]
		List<Horario> horarios = horariosRepo.findByFecha(fecha);
		peliculas = new LinkedList<>();

		// Formamos la lista final de Peliculas que regresaremos.
		for (Horario h : horarios) {
			// Solo nos interesa de cada registro de horario, el registro de pelicula.
			peliculas.add(h.getPelicula());
		}		
		return peliculas;
	}

	@Override
	public List<Pelicula> buscarActivas() {
		List<Pelicula> peliculas = null;
		peliculas = peliculaRepo.findByEstatus_OrderByTitulo("Activa");
		return peliculas;
	}

	

}
