package net.itinajero.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import net.itinajero.app.model.Pelicula;

@Repository
//Si no se especifica nombre del repositorio, Spring asume el nombre de la clase (camelCase): peliculasRepository
public interface PeliculasRepository extends JpaRepository<Pelicula, Integer>{
	
	// Listado de peliculas filtradas por estatus
	public List<Pelicula> findByEstatus_OrderByTitulo(String estatus);
}
