package net.itinajero.app.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.itinajero.app.model.Horario;

/** Marcamos esta clase como un Bean de tipo Repository en nuestro Root ApplicationContext.
	Nota: La anotacion @Repository es opcional ya que al extender la interfaz JpaRepository Spring 
	crea una instancia en nuestro Root ApplicationContext.
*/
@Repository
public interface HorariosRepository extends JpaRepository<Horario, Integer> {
	
	//Este metodo seria para buscar por id de horario y fecha. Necesitamos buscar por id de pelicula
	//List<Horario> findByIdAndFechaOrderByHora(int idPelicula, Date fecha);
	
	//Seria asi.
	List<Horario> findByPelicula_IdAndFechaOrderByHora(int idPelicula, Date fecha);
	
	@Query("select h from Horario h where h.fecha = :fecha and pelicula.estatus='Activa' group by h.pelicula order by pelicula.id asc")
	public List<Horario> findByFecha(@Param("fecha") Date fecha);
	
}
