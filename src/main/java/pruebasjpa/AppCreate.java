package pruebasjpa;

/*
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import net.itinajero.app.repository.NoticiasRepository; */
import net.itinajero.app.model.Noticia;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppCreate {

	public static void main(String[] args) {
		Noticia noticia = new Noticia();
		noticia.setTitulo("Naturaleza retoma la ciudad tras ausencia de humanos.");
		noticia.setDetalle("Reportes de animales vistos en la ciudad.");
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		
		//NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
		/** CRUDREPOSITORY INTERFACE */
		/*
		//SAVE - Nombre del bean seria el nombre de la clase repository (camelCase)
		repo.save(noticia);
		
		//FIND_BY_ID
		Optional<Noticia> n = repo.findById(1);
		System.out.println(n);
		System.out.println(n.get()); //Para obtener objeto Noticia que esta envuelto por Optional
		n = repo.findById(18); //No existe, retorna Optional.empty
		System.out.println(n);
		
		//UPDATE
		n = repo.findById(2); // Buscamos registro a modificar
		if(n.isPresent()) {
			Noticia n_update = n.get(); //Obtenemos objeto noticia y modificamos
			n_update.setTitulo("COVID-19 Pandemia real o ciencia ficcion.");
			n_update.setDetalle("COVID-19 originada en China y qu ataca la humanidad");
			//repo.save(n_update); //Guardamos nuevamente
		}
		
		//VERIFICAR SI EXISTE UN REGISTRO
		System.out.println("Id=1 existe?: " + repo.existsById(1));
		System.out.println("Id=5 existe?: " + repo.existsById(5));
		
		//DELETE
		if(repo.existsById(3)) {
			repo.deleteById(3); //arroja excepcion si id no existe
		}
		
		//COUNT
		long count = repo.count();
		System.out.println("count: " + count);
		
		//FIND ALL
		Iterable<Noticia> listado = repo.findAll();
		for (Noticia noti : listado) {
			System.out.println(noti);
		}
		
		//DELETE ALL
		//repo.deleteAll();
		
		//FIND ALL BY ID - VARIOS REGISTROS POR ID
		List<Integer> ids = new LinkedList<Integer>();
		ids.add(2);
		ids.add(4);
		
		Iterable<Noticia> listado2 = repo.findAllById(ids);
		for (Noticia notic : listado2) {
			System.out.println(notic);
		}
		// FIN CRUDREPOSITORY INTERFACE
		*/
		
		/*
		//FIND ALL. Con JpaRepository, el findAll no devuelve Optional<Noticia>
		List<Noticia> listado3 = repo.findAll();
		for (Noticia noticia2 : listado3) {
			System.out.println(noticia2);
		}
		
		//deleteAll (CrudRepository) ideal para borrar tablas con pocos registros.
		//deleteAllInBatch (JpaRepository) es mas eficiente para tablas con muchos registros.
		
		//FIND ALL SORT POR UN CAMPO
		listado3 = repo.findAll(Sort.by("fecha"));
		for (Noticia noticia2 : listado3) {
			System.out.println(noticia2);
		}
		//FIND ALL SORT POR 2 O MAS CAMPOS
		listado3 = repo.findAll(Sort.by("fecha").descending().and(Sort.by("estatus")));
		for (Noticia noticia2 : listado3) {
			System.out.println(noticia2);
		}
		//FIND ALL PAGINACION. si queremos la sig pag seria PageRequest.of(1, 2)
		Page<Noticia> p = repo.findAll(PageRequest.of(0, 2)); //pag 0 con 3 registros.
		for (Noticia noticia2 : p) {
			System.out.println(noticia2);
		}
		
		//FIND ALL PAGINACION y SORT. pag 0 con 2 registros y sort por fecha.
		Page<Noticia> page = repo.findAll(PageRequest.of(0, 2, Sort.by("fecha").descending())); 
		for (Noticia noticia2 : page) {
			System.out.println(noticia2);
		}
		*/
		
		//List<Noticia> listado;
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//listado = repo.findBy(); //similar a findAll
		
		//listado = repo.findByEstatus("Inactiva");
		
		/*listado = repo.findByEstatusOrderByFechaDesc("Inactiva");
		for (Noticia not : listado) {
			System.out.println(not);
		}*/
		
		/*try {
			listado = repo.findByFecha(sdf.parse("2020-04-23"));
			for (Noticia not : listado) {
				System.out.println(not);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}*/
		/*
		try {
			//listado = repo.findByEstatusAndFecha("Inactiva",sdf.parse("2020-04-23"));
			//listado = repo.findByEstatusOrFecha("Inactiva",sdf.parse("2020-04-23"));
			listado = repo.findByFechaBetweenOrderByFecha(sdf.parse("2020-04-19"),sdf.parse("2020-04-22"));
			for (Noticia not : listado) {
				System.out.println(not);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}*/
		
		
		
		
		context.close();
	}

}
