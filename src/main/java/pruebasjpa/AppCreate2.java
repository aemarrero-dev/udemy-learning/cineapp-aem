package pruebasjpa;

import java.util.List;
//import java.util.Optional;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import net.itinajero.app.model.Detalle;
import net.itinajero.app.model.Horario;
import net.itinajero.app.model.Pelicula;
import net.itinajero.app.repository.DetalleRepository;
import net.itinajero.app.repository.HorariosRepository;
import net.itinajero.app.repository.PeliculasRepository;

public class AppCreate2 {
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		
		PeliculasRepository repo = context.getBean("peliculasRepository", PeliculasRepository.class);
		DetalleRepository repoD = context.getBean("detalleRepository", DetalleRepository.class);
		
		HorariosRepository repoH = context.getBean("horariosRepository", HorariosRepository.class);
		
		List<Pelicula> lista;
		List<Detalle> listaD;
		List<Horario> listaH;
		
		
		lista = repo.findAll();
		for (Pelicula pelicula : lista) {
			System.out.println(pelicula.toString());
		}
		
		listaD = repoD.findAll();
		for (Detalle detalle : listaD) {
			System.out.println(detalle.toString());
		}
		
		listaH = repoH.findAll();
		for (Horario horario : listaH) {
			System.out.println(horario.toString());
		}
		
		//Consulto pelicula y se trae sus horarios.
		//Optional<Pelicula> n = repo.findById(3);
		//System.out.println(n.get().getHorarios().size());
		
		
		context.close();
		
	}
	
	
}
